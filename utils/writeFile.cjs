const fs = require("fs");

function writeFile(content, filename, callback) {
  fs.writeFile(filename, content, (err) => {
    if (err) {
      console.error(err);
      return;
    } else {
      callback(filename);
    }
  });
}

module.exports = writeFile;
