const fs = require("fs");

function deleteFile(filename, callback) {
  fs.unlink(filename, (err) => {
    if (err) {
      console.error(err);
      return;
    } else {
      callback(filename);
    }
  });
}

module.exports = deleteFile;
