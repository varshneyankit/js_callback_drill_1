const fs = require("fs");

function readFile(filename, callback) {
  fs.readFile(filename, "utf8", (err, data) => {
    if (err) {
      console.error(err);
      return;
    } else {
      callback(data);
    }
  });
}

module.exports = readFile;
