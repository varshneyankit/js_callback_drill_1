const fs = require("fs");

function appendFile(content, filename, callback) {
  fs.appendFile(filename, content, (err) => {
    if (err) {
      console.error(err);
      return;
    } else {
      callback(filename);
    }
  });
}

module.exports = appendFile;
