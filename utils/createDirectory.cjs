const fs = require("fs");

function createDirectory(folderName, callback) {
  try {
    if (!fs.existsSync(folderName)) {
      fs.mkdirSync(folderName);
      callback();
    }
  } catch (err) {
    console.error(err);
    return;
  }
}

module.exports = createDirectory;
