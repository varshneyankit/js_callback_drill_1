const createDirectory = require("./utils/createDirectory.cjs");
const writeFile = require("./utils/writeFile.cjs");
const deleteFile = require("./utils/deleteFile.cjs");

function createAndDeleteRandomFiles(directoryName) {
  createDirectory(directoryName, () => {
    for (let fileNumber = 1; fileNumber <= 5; fileNumber++) {
      const filename = `${directoryName}/random_file${fileNumber}.json`;
      writeFile("Some content!", filename, () => {
        console.log("Created random file");
        deleteFile(filename, (filename) => {
          console.log("Deleted random file");
        });
      });
    }
  });
}

module.exports = createAndDeleteRandomFiles;
