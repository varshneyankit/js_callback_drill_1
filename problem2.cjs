const readFile = require("./utils/readFile.cjs");
const writeFile = require("./utils/writeFile.cjs");
const appendFile = require("./utils/appendFile.cjs");
const deleteFile = require("./utils/deleteFile.cjs");

function callbackOperations() {
  readFile("lipsum.txt", (data) => {
    let uppercaseData = data.toUpperCase();

    writeFile(uppercaseData, "uppercase_data.txt", (filename) => {
      console.log("Created " + filename);

      writeFile(filename + "\n", "filenames.txt", (filename) => {
        readFile(filename, (data) => {
          let lowercaseData = data.toLowerCase();
          let paragraphs = lowercaseData.split("\n\n");
          let totalSentences = "";
          paragraphs.forEach((paragraph) => {
            let paragraphContent = paragraph.trim();
            let sentences = paragraphContent.split(".");
            sentences.forEach((sentence) => {
              let sentenceContent = sentence.trim();
              if (sentenceContent != "") {
                sentenceContent += ".\n";
                totalSentences += sentenceContent;
              }
            });
          });

          writeFile(totalSentences, "sentences.txt", (filename) => {
            console.log("Created " + filename);

            appendFile(filename + "\n", "filenames.txt", (filename) => {
              readFile("sentences.txt", (data) => {
                let sentences = data.split("\n");
                sentences.sort();
                let totalSortedSentences = "";
                sentences.forEach((sentence) => {
                  if (sentence != "") {
                    totalSortedSentences += sentence + "\n";
                  }
                });

                writeFile(
                  totalSortedSentences,
                  "sorted_sentences.txt",
                  (filename) => {
                    console.log("Created " + filename);

                    appendFile(filename + "\n", "filenames.txt", (filename) => {
                      readFile("filenames.txt", (data) => {
                        let filenames = data.split("\n");
                        filenames.forEach((filename) => {
                          if (filename != "") {
                            deleteFile(filename, (filename) => {
                              console.log("Deleted " + filename);
                            });
                          }
                        });
                      });
                    });
                  }
                );
              });
            });
          });
        });
      });
    });
  });
}

module.exports = callbackOperations;
